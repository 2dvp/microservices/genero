package com.fiap.genero;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "GENERO")
public class Genero {
   @Id
   @GeneratedValue
   public int id;
   public String name; 
   public int type;
   public String desc;

   public Integer getId() {
      return id;
  }

}