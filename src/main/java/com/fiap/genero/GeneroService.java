package com.fiap.genero;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneroService {
   @Autowired
   GeneroRepository generoRepository;

   public List<Genero> getAllGenre() {
       List<Genero> genero = new ArrayList<Genero>();
       GeneroRepository.findAll().forEach(genero -> generos.add(genero));
       return generos;
   }

   public Genero getGeneroById(int id) {
       return generoRepository.findById(id).get();
   }
}