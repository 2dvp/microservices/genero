package com.fiap.genero;

import org.springframework.data.repository.CrudRepository;

public interface GeneroRepository extends CrudRepository<Genero, Integer> {
   
}